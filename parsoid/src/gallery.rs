/*
Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//! <gallery> tags allow for displaying a collection of media,
//! they are core functionality in MediaWiki but treated as an
//! extension in Parsoid.
//!
//! See the [specification](https://www.mediawiki.org/wiki/Specs/HTML/2.2.0/Extensions/Gallery) for more details.

use crate::image::Image;
use crate::{assert_element, inner_data, set_inner_data, Result, Wikinode};
use indexmap::IndexMap;
use kuchiki::NodeRef;
use serde::{Deserialize, Serialize};

/// Represents a gallery tag (`<gallery>`)
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.2.0/Extensions/Gallery) for more details.
#[derive(Debug, Clone)]
pub struct Gallery(pub(crate) NodeRef);

impl Gallery {
    pub(crate) const TYPEOF: &'static str = "mw:Extension/gallery";

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    /// Get a map of the attributes set on the `<gallery>` tag
    pub fn attributes(&self) -> Result<IndexMap<String, String>> {
        Ok(self.inner()?.attrs)
    }

    /// Set an attribute on the `<gallery>` tag. The previous value,
    /// if one was set, is returned.
    pub fn set_attribute(
        &self,
        name: &str,
        value: &str,
    ) -> Result<Option<String>> {
        let mut inner = self.inner()?;
        let old = inner.attrs.insert(name.to_string(), value.to_string());
        self.set_inner(inner)?;
        Ok(old)
    }

    /// Remove an attribute from the `<gallery>` tag. If set, the previous
    /// value is returned.
    pub fn remove_attribute(&self, name: &str) -> Result<Option<String>> {
        let mut inner = self.inner()?;
        let old = inner.attrs.remove(name);
        self.set_inner(inner)?;
        Ok(old)
    }

    /// Images contained in this gallery. Note, does not include other
    /// media like audio or video files.
    pub fn images(&self) -> Vec<Image> {
        self.inclusive_descendants()
            .filter_map(|node| Wikinode::new_from_node(&node).as_image())
            .collect()
    }

    /// Get the wikitext in between the `<gallery>` and `</gallery>` tags
    pub fn wikitext(&self) -> Result<String> {
        // Trim leading and trailing whitespace for convenience
        Ok(self.inner()?.body.extsrc.trim().to_string())
    }

    /// Set the wikitext in between the `<gallery>` and `</gallery>` tags
    pub fn set_wikitext(&self, wikitext: &str) -> Result<()> {
        let mut data = self.inner()?;
        // Normalize whitespace for consistency
        data.body.extsrc = format!("\n{wikitext}\n");
        self.set_inner(data)?;
        Ok(())
    }

    fn inner(&self) -> Result<GalleryDataMw> {
        inner_data(self)
    }

    fn set_inner(&self, data: GalleryDataMw) -> Result<()> {
        set_inner_data(self, data)
    }
}

#[derive(Deserialize, Serialize)]
pub(crate) struct GalleryDataMw {
    pub(crate) name: String,
    pub(crate) attrs: IndexMap<String, String>,
    pub(crate) body: GalleryBody,
}

#[derive(Deserialize, Serialize)]
pub(crate) struct GalleryBody {
    pub(crate) extsrc: String,
}
