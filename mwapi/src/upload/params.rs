/*
Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::params::Params;
use reqwest::multipart;
use std::collections::HashMap;

pub(crate) struct MultipartParams {
    pub(crate) params: Params,
    pub(crate) parts: HashMap<String, multipart::Part>,
}

impl MultipartParams {
    pub(crate) async fn into_form(self) -> crate::Result<multipart::Form> {
        let mut form = multipart::Form::new();
        for (name, part) in self.parts {
            form = form.part(name, part);
        }
        for (name, value) in self.params.map {
            form = form.text(name, value);
        }
        Ok(form)
    }
}
