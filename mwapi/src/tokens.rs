/*
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::responses::TokenResponse;
use crate::{Client, Error, Result};
use std::collections::HashMap;

type Token = String;

#[derive(Debug, Default)]
pub(crate) struct TokenStore {
    map: HashMap<String, Token>,
}

impl TokenStore {
    /// Get a token that's already loaded. It's up to the caller
    /// to lazy-load the token as a fallback and gracefully
    /// handle expired tokens
    pub(crate) fn get(&self, name: &str) -> Option<Token> {
        self.map.get(name).map(|token| token.to_string())
    }

    pub(crate) async fn load(
        &mut self,
        name: &str,
        api: &Client,
    ) -> Result<Token> {
        let resp: TokenResponse = api
            .get(&[("action", "query"), ("meta", "tokens"), ("type", name)])
            .await?;
        match resp.query.tokens.get(&format!("{name}token")) {
            Some(token) => {
                self.map.insert(name.to_string(), token.to_string());
                Ok(token.to_string())
            }
            None => Err(Error::TokenFailure(name.to_string())),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[tokio::test]
    async fn test_tokenstore() {
        let mut store = TokenStore::default();
        assert_eq!(store.get("csrf"), None);
        let client = Client::new("https://test.wikipedia.org/w/api.php")
            .await
            .unwrap();
        let fetched = store.load("csrf", &client).await.unwrap();
        assert_eq!(&fetched, "+\\");
        assert_eq!(store.get("csrf"), Some("+\\".to_string()));
        let err = store.load("invalid", &client).await.unwrap_err();
        assert!(matches!(err, Error::TokenFailure(_)));
    }
}
