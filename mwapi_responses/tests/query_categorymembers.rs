// Autogenerated by gen_tests.py
use mwapi_responses::prelude::*;

mod test_client;

#[query(action = "query", list = "categorymembers", cmprop = "ids")]
struct Response0;

#[tokio::test]
async fn query_categorymembers_ids() {
    let mut params = Response0::params().to_vec();
    params.push(("cmtitle", "Category:Physics"));
    let resp: Response0 = test_client::test(&params).await.unwrap();
    assert_eq!(resp.items().len(), 10);
    assert!(!resp.continue_.is_empty());
}

#[query(action = "query", list = "categorymembers", cmprop = "sortkey")]
struct Response1;

#[tokio::test]
async fn query_categorymembers_sortkey() {
    let mut params = Response1::params().to_vec();
    params.push(("cmtitle", "Category:Physics"));
    let resp: Response1 = test_client::test(&params).await.unwrap();
    assert_eq!(resp.items().len(), 10);
    assert!(!resp.continue_.is_empty());
}

#[query(action = "query", list = "categorymembers", cmprop = "sortkeyprefix")]
struct Response2;

#[tokio::test]
async fn query_categorymembers_sortkeyprefix() {
    let mut params = Response2::params().to_vec();
    params.push(("cmtitle", "Category:Physics"));
    let resp: Response2 = test_client::test(&params).await.unwrap();
    assert_eq!(resp.items().len(), 10);
    assert!(!resp.continue_.is_empty());
}

#[query(action = "query", list = "categorymembers", cmprop = "timestamp")]
struct Response3;

#[tokio::test]
async fn query_categorymembers_timestamp() {
    let mut params = Response3::params().to_vec();
    params.push(("cmtitle", "Category:Physics"));
    let resp: Response3 = test_client::test(&params).await.unwrap();
    assert_eq!(resp.items().len(), 10);
    assert!(!resp.continue_.is_empty());
}

#[query(action = "query", list = "categorymembers", cmprop = "title")]
struct Response4;

#[tokio::test]
async fn query_categorymembers_title() {
    let mut params = Response4::params().to_vec();
    params.push(("cmtitle", "Category:Physics"));
    let resp: Response4 = test_client::test(&params).await.unwrap();
    assert_eq!(resp.items().len(), 10);
    assert!(!resp.continue_.is_empty());
}

#[query(action = "query", list = "categorymembers", cmprop = "type")]
struct Response5;

#[tokio::test]
async fn query_categorymembers_type() {
    let mut params = Response5::params().to_vec();
    params.push(("cmtitle", "Category:Physics"));
    let resp: Response5 = test_client::test(&params).await.unwrap();
    assert_eq!(resp.items().len(), 10);
    assert!(!resp.continue_.is_empty());
}
