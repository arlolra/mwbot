use std::path::PathBuf;

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use mwtitle::{NamespaceMap, Title, TitleCodec};

pub fn parse_title(c: &mut Criterion) {
    let title_codec = TitleCodec::from_path(&PathBuf::from_iter([
        std::env::var_os("CARGO_MANIFEST_DIR")
            .expect("CARGO_MANIFEST_DIR should be set when benchmarks are run"),
        "benches/enwiki-siteinfo-namespaces-interwikimap.json".into(),
    ]))
    .expect("valid siteinfo-namespaces-interwikimap.json");
    c.bench_function("parse_mainspace_title", |b| {
        b.iter(|| title_codec.new_title(black_box("Main Page")));
    });
    c.bench_function("parse_project_title", |b| {
        b.iter(|| title_codec.new_title(black_box("Wikipedia:About")));
    });
}

pub fn format_title(c: &mut Criterion) {
    let namespace_map = NamespaceMap::from_path(&PathBuf::from_iter([
        std::env::var_os("CARGO_MANIFEST_DIR")
            .expect("CARGO_MANIFEST_DIR should be set when benchmarks are run"),
        "benches/enwiki-siteinfo-namespaces.json".into(),
    ]))
    .expect("valid siteinfo-namespaces.json");
    let title = unsafe { Title::new_unchecked(0, "Main Page".into()) };
    c.bench_function("to_pretty_main", |b| {
        b.iter(|| namespace_map.to_pretty(black_box(&title)));
    });
    let title = unsafe { Title::new_unchecked(4, "About".into()) };
    c.bench_function("to_pretty_project", |b| {
        b.iter(|| namespace_map.to_pretty(black_box(&title)));
    });
}

pub fn title_roundtrip(c: &mut Criterion) {
    let title_codec = TitleCodec::from_path(&PathBuf::from_iter([
        std::env::var_os("CARGO_MANIFEST_DIR")
            .expect("CARGO_MANIFEST_DIR should be set when benchmarks are run"),
        "benches/enwiki-siteinfo-namespaces-interwikimap.json".into(),
    ]))
    .expect("valid siteinfo-namespaces-interwikimap.json");
    c.bench_function("roundtrip_mainspace_title", |b| {
        b.iter(|| {
            let pretty_title = "Main Page";
            let title = title_codec.new_title(black_box(pretty_title)).unwrap();
            black_box(title_codec.to_pretty(&title));
        });
    });
    c.bench_function("roundtrip_project_title", |b| {
        b.iter(|| {
            let pretty_title = "Wikipedia:About";
            let title = title_codec.new_title(black_box(pretty_title)).unwrap();
            black_box(title_codec.to_pretty(&title));
        });
    });
}

criterion_group!(benches, parse_title, format_title, title_roundtrip);
criterion_main!(benches);
